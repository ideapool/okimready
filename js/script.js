var v0 = false;
    
function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}

$(document).ready(function() {

	$('.fm_bt').click(function() {
		if ($('.inp_em').val() === '') {
			$('.inp_em').css({
				'border': '1.5px solid rgb(245, 137, 137)',
				'background-color': '#ffeaea'
			})
		}

		if ($('.inp_em').val() !== '') {
			var em = $('.inp_em').val();
			if(!validateEmail(em)) {
				$('.inp_em').css({
					'border': '1.5px solid rgb(245, 137, 137)',
					'background-color': '#ffeaea'
				})
			} else {
				$.ajax({
					  type: "POST",
					  url: "mail.php",
					  data: {'email': em},
						success: function() {
							$('.mn4brt1').fadeOut(0);
							$('.mn4brt1_em').fadeIn(0);
							$('.form').animate({
						    'opacity': '0'
						  }, 500);
						}
				});
			}
		}
	})



  $('.mn_t').animate({
    'padding-top': '200px',
    'opacity': '1'
  }, 1000);
  $('.ph_img').animate({
    'opacity': '1',
    'margin-top': '40px'
  }, 1000);

	var hgt = $('.mn').height() + $('.mn2').height();
	
	/*
$('.logo1').fadeIn(0);
	$('.logo2').fadeOut(0);
	$('.header').css("background-color", "#FFFDF7");
*/


  $(window).scroll(function() {
    
    if ($(window).scrollTop() < $('.mn').height()) {
      $('.logo1').fadeIn(0);
      /* $('.logo2').fadeOut(0); */
      $('.header').css("background-color", "#FFFDF7");
    }
    
    //second block
    if ($(window).scrollTop() >= $('.mn').height() - 250) {
      /*
$('.logo1').fadeOut(0);
      $('.logo2').fadeIn(0);
*/
      $('.header').css("background-color", "rgba(255,253,247,0.5)");
      $('.mn2b').animate({
        'padding-top': '160px',
        'opacity': '1'
      }, 1000);
    }


	//block three
	if ($(window).scrollTop() >= hgt - 250) {
      /*
$('.logo1').fadeOut(0);
      $('.logo2').fadeOut(0);
      $('.logo3').fadeIn(0);
*/
      $('.header').css("background-color", "rgba(255,255,255,0.8)");
      $('.mn_t_3').animate({
		    'padding-top': '200px',
		    'opacity': '1'
	  }, 1000);
      $('.ph_img_3').animate({
	    'opacity': '1',
	    'top': '400px'
	  }, 1000);
	  
	  $('.mn3').delay(1000).fadeOut(1000);
    }

	
	if ($(window).scrollTop() < hgt + 300 && v0 == true) {
	    /* $('.logo3').removeClass().addClass('logo3'); */
	    
	    /* $('.mn3').fadeIn(100); */
		v0 = false;
    }
    
    if($(window).scrollTop() >= hgt + 300){
	    v0 = true;
    }


    if ($(window).scrollTop() >= hgt + 300 && $(window).scrollTop() < hgt + 700) {
	    $('.fixed_ad').removeClass('dnone');
	    /* $('.logo3').addClass('logo3_2'); */
	    /*
$('.mn3').fadeOut(1000, function(){
			$('.mn3_2').fadeIn(100);
		});
*/
	    
    }

/*
    if ($(window).scrollTop() >= hgt + 700 && $(window).scrollTop() < hgt + 1100) {
		$('.logo3').removeClass().addClass('logo3 logo3_3');
		    $('.mn3,.mn3_2,.mn3_4,.mn3_5,.mn3_6').fadeOut(25, function(){$('.mn3_3').fadeIn(200);});
	    
    }


    if ($(window).scrollTop() >= hgt + 1100 && $(window).scrollTop() < hgt + 1500) {
		$('.logo3').removeClass().addClass('logo3 logo3_4');
		    $('.mn3,.mn3_2,.mn3_3,.mn3_5,.mn3_6').fadeOut(25, function(){$('.mn3_4').fadeIn(200);});

    }

    if ($(window).scrollTop() >= hgt + 1500 && $(window).scrollTop() < hgt + 1800) {
		$('.logo3').removeClass().addClass('logo3 logo3_5');
	    $('.mn3,.mn3_2,.mn3_3,.mn3_4,.mn3_6').fadeOut(25, function(){$('.mn3_5').fadeIn(200);});


    }

    if ($(window).scrollTop() >= hgt + 1900 && $(window).scrollTop() < hgt + 2300) {
		$('.logo3').removeClass().addClass('logo3 logo3_6');
	    $('.mn3,.mn3_2,.mn3_3,.mn3_4,.mn3_5').fadeOut(25, function(){$('.mn3_6').fadeIn(200);});
    }
*/







    //block four
    if ($(window).scrollTop() >= hgt + hgt/2 - 250) {
      /*
$('.logo1').fadeOut(0);
      $('.logo2').fadeOut(0);
      $('.logo3').fadeOut(0);
      $('.logo4').fadeIn(0);
*/
      $('.header').css("background-color", "rgba(255,255,255,0.8)");
      $('.mn4b').animate({
        'padding-top': '150px',
        'opacity': '1'
      }, 1000);
    }

    if ($(window).scrollTop() < $('.mn').height() +  $('.mn2').height() + hgt/2 - 250) {
      /*
$('.logo4').fadeOut(0);
      $('.logo3').fadeIn(0);
*/
    }

    if ($(window).scrollTop() < $('.mn').height() + $('.mn2').height() - 250) {
      /* $('.logo3').fadeOut(0); */
    }


  });
  
	$('.arf,.arfm').click(function(){
		$('.mn3_cont').animate({
			backgroundColor: $(this).attr('data-color')
		}, 1000);
		
		$( ".fixed_ad" ).animate({
			left: "-=20%"
		}, 1000);
	});
	
	$('.arb,.arbm').click(function(){
		$('.mn3_cont').animate({
			backgroundColor: $(this).attr('data-color')
		}, 1000);
		$( ".fixed_ad" ).animate({
			left: "+=20%"
		}, 1000);
	});
});
